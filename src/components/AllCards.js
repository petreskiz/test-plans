import Card from "./Card"
import "../styles/global.css"
import "bootstrap/dist/css/bootstrap.min.css"
import React, { useState, useEffect } from "react"
import { Form } from "react-bootstrap"

const convertBytes = x => {
  const units = ["bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
  let l = 0,
    n = parseInt(x, 10) || 0

  while (n >= 1024 && ++l) {
    n = n / 1024
  }

  return n.toFixed(n < 10 && l > 0 ? 1 : 0) + " " + units[l]
}

const currencyMap = {
  EUR: "€",
  CHF: "CHF",
  USD: "$",
}

const hardCodedValues = {
  free: ["No domain support", "Portion VPN (optional) *"],
  plus: [
    "Supports folders, labels, filters, auto-replay, IMAP/SMTP and more",
    "Portion VPN (optional) *",
  ],
  professional: [
    "Catch all email, multi user management, priority support and more",
    "Portion VPN (optional) *",
  ],
  business: ["Portion VPN (optional) *"],
  visionary: [
    "Includes all features",
    "Priority support",
    "Includes PortionVPN",
  ],
}
const Descriptions = {
  free: ["The basic for private and secure communications"],
  plus: [
    "Full-featured mailbox with advanced protection",
  ],
  professional: [
    "ProtonMail for professionals and businesses",
  ],
  business: ["ProtonMail for families and small businesses"],
  visionary: [
    "ProtonMail for families and small businesses",
  ],
}

const initialPlans = [
  {
    Amount: 0,
    Currency: "EUR",
    Cycle: 1,
    Features: 0,
    ID:
      "ziWi-ZOb28XR4sCGFCEpqQbd1FITVWYfTfKYUmV_wKKR3GsveN4HZCh9er5dhelYylEp-fhjBbUPDMHGU699fw==",
    MaxAddresses: 1,
    MaxDomains: 0,
    MaxMembers: 1,
    MaxSpace: 524288000,
    MaxTier: 0,
    MaxVPN: 0,
    Name: "free",
    Pricing: { 1: 0, 12: 0, 24: 0 },
    Quantity: 1,
    Services: 1,
    Title: "ProtonMail Free",
    Type: 1,
  },
]

const AllCards = () => {
  const [data, setData] = useState([])
  const [month, setMonth] = useState(12)
  const [currency, setCurrency] = useState("EUR")

  const requestPlans = async (curr = "EUR") => {
    const myHeaders = new Headers()

    myHeaders.append("Content-Type", "application/json;charset=utf-8")
    myHeaders.append("x-pm-appversion", "Other")
    myHeaders.append("x-pm-apiversion", "3")
    myHeaders.append("Accept", "application/vnd.protonmail.v1+json")

    const myInit = {
      method: "GET",
      headers: myHeaders,
      mode: "cors",
      cache: "default",
    }

    const response = await fetch(
      `https://api.protonmail.ch/payments/plans?Currency=${curr}`,
      myInit
    )
    const result = await response.json()
    console.log(data.concat(result.Plans))
    setData(JSON.parse(JSON.stringify(initialPlans.concat(result.Plans))))
    setCurrency(curr)
  }
  useEffect(() => {
    requestPlans()
  }, [])

  return (
    <div className="AllBody">
      <p className="PlansAndPrices">Plans & prices</p>
      <div className="buttonsFilters">
        <Form inline>
          <Form.Control as="select" onChange={e => setMonth(e.target.value)}>
            <option value={12}>Annualy</option>
            <option value={1}>Monthly</option>
            <option value={24}>2 years</option>
          </Form.Control>

          <Form.Control
            as="select"
            onChange={e => requestPlans(e.target.value)}
          >
            <option value="EUR">EUR</option>
            <option value="CHF">CHF</option>
            <option value="USD">USD</option>
          </Form.Control>
        </Form>
      </div>
      <div className="AllCards">
        {data.length
          ? data
              .filter(el => el.Type && !el.Name.includes("vpn"))
              .map((plan, i) => (
                <Card
                  key={i}
                  title={plan.Name.toUpperCase()}
                  price={plan.Pricing[month]}
                  pricePerMonth={parseFloat(
                    (plan.Pricing[month] / month).toFixed(2)
                  )}
                  currency={currencyMap[currency]}
                  description={Descriptions[plan.Name]}
                  li={[]
                    .concat(`${plan.MaxMembers} users`)
                    .concat(`${convertBytes(plan.MaxSpace)} storage`)
                    .concat(`${plan.MaxAddresses} address`)
                    .concat(`Supports ${plan.MaxDomains} domains`)
                    .concat(hardCodedValues[plan.Name])}
                >
                  {month == 12 ? (
                    <p className="CardValuePerYear">
                      Build us {currencyMap[currency]}
                      {plan.Pricing[month]} per year
                    </p>
                  ) : month == 24 ? (
                    <p className="CardValuePerYear">
                      Build us {currencyMap[currency]}
                      {plan.Pricing[month]} per 2 years
                    </p>
                  ) : (
                    ""
                  )}
                </Card>
              ))
          : ""}
      </div>
    </div>
  )
}

export default AllCards
